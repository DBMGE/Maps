const fs = require('fs');
const vmf = require('vmfparser');
const glob = require('glob');
const path = require('path');
const sprintf = require('sprintf-js').sprintf;

glob('../maps/**/*.vmf', (err, files) => {
  if (err) throw err;

    let promises = [];

    for (file of files)
      promises.push(validateMap(file));

    Promise.all(promises).then(values => {
      for (value of values)
        console.log(value);
    }).catch(err => {
      console.log(err);
      process.exit(1);
    })
});

function validateMap(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, (err, data) => {
      if (err) return reject(err);

      file = path.basename(file);

      let spawn = false;

      let map = {};

      let entities = vmf(data.toString())['entity'];

      // Create parent data
      for (entity of entities) {
        if (entity.classname == 'info_target') {
          let register = entity.targetname.match("registerarena_([A-z0-9]+)");

          if (register != null) {
            map[register[1]] = {
              registered: true,
              rocketspawn: false,
              teamsize: -1,
              teamspawn: {
                blue: 0,
                red: 0
              }
            };
          }
        } else if (entity.classname == 'info_player_teamspawn')
          spawn = true;
      }

      if (!spawn)
        return reject(sprintf('%s has no spawn point', file));

      if (Object.keys(map).length <= 0)
        return reject(sprintf('%s has no registered arena', file));

      // Collect child data
      for (entity of entities) {
        if (entity.classname == 'info_target') {
          let rs = entity.targetname.match("([A-z0-9]+)_rocketspawn");
          let ts = entity.targetname.match("([A-z0-9]+)_teamsize:([0-9]+)");
          let tss = entity.targetname.match("([A-z0-9]+)_spawn_(red|blue)_([0-9]+)");

          if (rs != null) {
            if (!map.hasOwnProperty(rs[1]))
              return reject(sprintf('%s: %s has no parent registration', file, rs[1]));

            map[rs[1]].rocketspawn = true;
          }

          if (ts != null) {
            if (!map.hasOwnProperty(ts[1]))
              return reject(sprintf('%s: %s has no parent registration', file, ts[1]));

            map[ts[1]].teamsize = ts[2];
          }

          if (tss != null) {
            if (!map.hasOwnProperty(tss[1]))
              return reject(sprintf('%s: %s has no parent registration', file, tss[1]));

            map[tss[1]]['teamspawn'][tss[2]]++;
          }
        }
      }

      // Finally validate all collected data
      let arenas = Object.keys(map);

      if (arenas.length <= 0)
        return reject(sprintf('%s has no arenas', file));

      for (arena of arenas) {
        if (!map[arena].rocketspawn)
          return reject(sprintf('%s: %s has no rocket spawn point', file, arena));

        if (map[arena].teamsize <= 0)
          return reject(sprintf('%s: %s has an invalid team size', file, arena));

        if (map[arena].teamsize != map[arena].teamspawn.blue)
          return reject(sprintf('%s: %s has an invalid amount of blue spawns', file, arena));

        if (map[arena].teamsize != map[arena].teamspawn.red)
          return reject(sprintf('%s: %s has an invalid amount of red spawns', file, arena));
      }

      resolve(sprintf("%s Passed", file));
    })
  });
}
