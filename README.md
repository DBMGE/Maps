# DBMGE Map Requirement

Each map should contain 2 or more arenas but no more than 8 for efficiency reasons

## Requirement For The Map

### Spawn Room

  This room's purpose is to serve as a point of spawn and immediately being teleported

  The player will never see the room, as they're teleported right after respawn

  - Single Player Spawn `info_player_teamspawn` with `Team` set to `Any`

## Requirement For Each Arena

### Arena Registration

  This `info_target` will register the arena and put it in rotation

  - Single Arena Registration Entity `info_target` (`registerarena_arenaname`)
    * Ex: `registerarena_octagon`

### Rocket Spawn

  This `info_target` will tell the plugin where to spawn the rocket for each arena

  - Single Rocket Spawn `info_target` (`arenaname_rocketspawn`)
    * Ex: `octagon_rocketspawn`

### Player Spawn Count

  This entity will be used to store the number of players per team on the arena

  - Single Player Spawn Count Entity `info_target` (`arenaname_teamsize:size`)
    * Ex: `octagon_teamsize:2` (Replace `size` after `:` with a number)

### Player Spawns

  These entities will be used for teleporting players in

  The amount of entities should be `arenaname_teamsize:size` * 2 (Red & Blue)

  - Player Spawn Entity `info_target` (`arenaname_spawn_team_num`)
    * Ex:
      - Blue
        * `octagon_spawn_blue_1`
        * `octagon_spawn_blue_2`
      - Red
        * `octagon_spawn_red_1`
        * `octagon_spawn_red_2`
